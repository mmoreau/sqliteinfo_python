# SQLiteInfo_Python

**Scans files** with the extension "**sqlite**" or "**db**" by retrieving **table names**, **column names** from the **table**.

Saves time when we want to **search** for **information** on a **table**, on the **names** of **columns** in the table of a "**sqlite**" or "**db**" file.

## Example
The file "**script.py**" shows you an example on how you can use the script.

## Todo
* [ ] MySQL
* [ ] MariaDB
* [ ] PostgreSQL