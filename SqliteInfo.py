import os.path
import sqlite3

class SqliteInfo:


	SIGNATURE_HEX = "53 51 4C 69 74 65 20 66 6F 72 6D 61 74 20 33"
	SIGNATURE_STRING = "SQLite format 3"
	SIGNATURE_BYTE = b"SQLite format 3"


	@classmethod
	def isSignature(cls, path):

		"""Check if the file has the correct signature of a SQLite file."""

		if isinstance(path, str):
			if os.path.exists(path):
				with open(path, "rb") as f:
					return " ".join(map(str, [hex(character)[2:].upper() for character in f.read(15)])) == SqliteInfo.SIGNATURE_HEX

		return False



	@classmethod
	def tables(cls, path):

		"""Retrieves table names stored in a "sqlite" or "db" file."""

		if SqliteInfo.isSignature(path):
			try:
				conn = sqlite3.connect(path)
				cursor = conn.cursor()
				cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
				rows = cursor.fetchall()

				return {
					"path": path,
					"tables": [name for row in rows for name in row]
				}
			except:
				pass
					
		return {}



	@classmethod
	def columns(cls, path, table_name):

		"""Retrieves the names of the columns of a table stored in a "sqlite" or "db" file."""

		lock = 0


		if SqliteInfo.isSignature(path):
			lock += 1


		if isinstance(table_name, str):
			lock += 1


		if lock == 2:
			try:
				conn = sqlite3.connect(path)
				cursor = conn.cursor()
				cursor.execute("pragma table_info({})".format(table_name))
				rows = cursor.fetchall()

				return {
					"path": path,
					"table": table_name, 
					"columns": [row[1] for row in rows]
				}
			except:
				pass

		return {}



	@classmethod
	def datas(cls, path, table_name):

		"""Retrieves all data from the table."""

		lock = 0

		if SqliteInfo.isSignature(path):
			lock += 1


		if isinstance(table_name, str):
			lock += 1


		if lock == 2:
			try:
				conn = sqlite3.connect(path)
				cursor = conn.cursor()
				cursor.execute("SELECT * FROM {}".format(table_name))
				rows = cursor.fetchall()

				return {
					"path": path,
					"table": table_name, 
					"datas": [row for row in rows]
				}
			except:
				pass

		return {}