#!/usr/bin/python3

import os.path
from SqliteInfo import SqliteInfo

# .:: Retrieves table names ::.

"""
    # You need the path to a file with the extension "sqlite" or "db"

    SqliteInfo.tables("file.sqlite")
    SqliteInfo.tables("file.db")
    SqliteInfo.tables(r"C:\folder\file.sqlite")
    SqliteInfo.tables(r"/folder/file.sqlite")
"""

path = "myfile.sqlite"

getTables = SqliteInfo.tables(path)

# .:: Retrieves the names of the columns from the table ::.
if getTables:

	for table in getTables["tables"]:
    
        # .:: Retrieves the names of the columns from the table ::.
		getColumns = SqliteInfo.columns(getTables["path"], table)
        
		if getColumns:
			print("Path    :", getColumns["path"])
			print("Table   :", getColumns["table"])
			print("Columns :")

			[print("\t", columns_name) for columns_name in getColumns["columns"]]


		# .:: Retrieves all data from the table ::.
		getDatas = SqliteInfo.datas(getTables["path"], table)

		if getDatas:
			print("\nDatas :")

			[print("\t", data) for data in getDatas["datas"]]

		print("\n", "-"*50, "\n")